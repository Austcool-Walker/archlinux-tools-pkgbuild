# Maintainer: artoo <artoo@manjaro.org>
# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Bernhard Landauer <oberon@manjaro.org>

pkgbase=archlinux-tools
pkgname=('archlinux-tools-base'
        'archlinux-tools-pkg'
        'archlinux-tools-iso'
        'archlinux-tools-yaml')
pkgver=8fb71bf1a540c5cf5e98f2c530687a9db2415be9
pkgrel=4
arch=('i686' 'x86_64')
pkgdesc='Development tools for archlinux Linux'
license=('GPL')
groups=("${pkgbase}")
url="https://gitlab.com/Austcool-Walker/${pkgbase}"
makedepends=('docbook2x')
source=("${pkgbase}-${pkgver}.tar.gz::${url}/-/archive/master/{pkgbase}.tar.gz")
sha256sums=('SKIP')

prepare() {
    #mv manjaro-tools-stable-0.15.x ${pkgbase}-${pkgver}
    cd ${srcdir}/${pkgbase}-master-${pkgver}
    # patches here
#    patch -p1 -i ${srcdir}/grub-show-once.patch
#    patch -p1 -i ${srcdir}/ip-service.patch
#    patch -p1 -i ${srcdir}/update-ip-service.patch
#    patch -p1 -i ${srcdir}/kde-ip-service.patch
#    patch -p1 -i ${srcdir}/nvidia304.patch
#    patch -p1 -i ${srcdir}/update-xorriso-cmds-for-v1.4.8.patch
#    patch -p1 -i ${srcdir}/use-full-iso9660-filenames.patch
#    patch -p1 -i ${srcdir}/use-linux419-by-default.patch
#    patch -p1 -i ${srcdir}/update-geoip-service.patch

    sed -e "s/^Version=.*/Version=${pkgver}/" -i Makefile
    if [ "${CARCH}" = "i686" ]; then
        patch -p2 -i ${srcdir}/manjaro-32.patch
    fi
}

build() {
    cd ${srcdir}/${pkgbase}-master-${pkgver}
    make SYSCONFDIR=/etc PREFIX=/usr
}

package_archlinux-tools-base() {
    pkgdesc='Development tools for Manjaro Linux (base tools)'
    depends=('openssh' 'rsync' 'haveged' 'os-prober' 'gnupg' 'pacman-mirrorlist>=20160301')
    optdepends=('archlinux-tools-pkg: Manjaro Linux package tools'
                'archlinux-tools-iso: Manjaro Linux iso tools'
                'archlinux-tools-yaml: Manjaro Linux yaml tools')
    backup=('etc/manjaro-tools/manjaro-tools.conf')

    cd ${srcdir}/${pkgbase}-master-${pkgver}
    make SYSCONFDIR=/etc PREFIX=/usr DESTDIR=${pkgdir} install_base
}

package_archlinux-tools-pkg() {
    pkgdesc='Development tools for Manjaro Linux (packaging tools)'
    depends=('namcap' 'archlinux-tools-base')
    conflicts=('devtools')

    cd ${srcdir}/${pkgbase}-master-${pkgver}
    make SYSCONFDIR=/etc PREFIX=/usr DESTDIR=${pkgdir} install_pkg
}

package_archlinux-tools-yaml() {
    pkgdesc='Development tools for Manjaro Linux (yaml tools)'
    depends=('archlinux-tools-base' 'calamares-tools' 'ruby-kwalify' 'manjaro-iso-profiles-base')

    cd ${srcdir}/${pkgbase}-master-${pkgver}
    make SYSCONFDIR=/etc PREFIX=/usr DESTDIR=${pkgdir} install_yaml
}

package_archlinux-tools-iso() {
    pkgdesc='Development tools for Manjaro Linux (ISO tools)'
    depends=('dosfstools' 'libisoburn' 'squashfs-tools' 'archlinux-tools-yaml'
            'mkinitcpio' 'mktorrent' 'grub')
    optdepends=('qemu: quickly test isos')

	cd ${srcdir}/${pkgbase}-master-${pkgver}
	make SYSCONFDIR=/etc PREFIX=/usr DESTDIR=${pkgdir} install_iso
}
